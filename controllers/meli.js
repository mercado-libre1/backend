const axios = require('axios');

const searchItemsML = async (query) => {
    if (!query) {
        throw Error("query param required")
    }

    const result = await axios.get(`https://api.mercadolibre.com/sites/MLA/search?q=${query}&limit=4`);
    const categories = result
        .data
        .filters[0]
        .values[0]
        .path_from_root.map(c => c.name);

    return { results: result.data.results, categories };
}

const getItemDetailsML = async (id) => {
    if (!id) {
        throw Error("id param required")
    }
    const result = await axios.get(`https://api.mercadolibre.com/items/${id}/`);
    const categories = await getItemCategoriesML(result.data.category_id);

    return { ...result.data, categories };
}

const getItemCategoriesML = async (category_id) => {
    if (!category_id) {
        throw Error("category_id is required");
    }
    const result = await axios.get(`https://api.mercadolibre.com/categories/${category_id}/`);

    return result.data.path_from_root.map(c => c.name);
}

const getItemDescriptionML = async (id) => {
    if (!id) {
        throw Error("id param required")
    }

    const result = await axios.get(`https://api.mercadolibre.com/items/${id}/description`);
    return result.data;
}

module.exports = { searchItemsML, getItemDetailsML, getItemCategoriesML, getItemDescriptionML };