const express = require('express');
const router = express.Router();
const {
    searchItemsML,
    getItemDetailsML,
    getItemDescriptionML
} = require('./meli');

router.get('/', async (req, res) => {

    const { q } = req.query;
    if (q) {
        try {
            const products = await searchItemsML(q);
            const payload = {
                author: {
                    name: 'Gabriel',
                    lastname: 'Tortomano'
                },
                categories: products.categories,
                items: products.results.map(p => {
                    return {
                        id: p.id,
                        title: p.title,
                        picture: p.thumbnail,
                        condition: p.condition,
                        free_shipping: p.shipping.free_shipping,
                        address: p.address.state_name,
                        price: {
                            currency: p.currency_id,
                            amount: p.price
                        }
                    }
                })
            };
            res.send(payload);
        } catch (e) {
            res.status(400).send(e);
        }
    } else {
        res.status(204).send();
    }
});


router.get('/:id', async (req, res) => {
    const { id } = req.params;
    if (id) {
        try {
            const product = getItemDetailsML(id);
            const description = getItemDescriptionML(id);
            const result = await Promise.all([product, description]);
            const payload = {
                author: {
                    name: 'Gabriel',
                    lastname: 'Tortomano'
                },
                categories: result[0].categories,
                item: {
                    id: result[0].id,
                    title: result[0].title,
                    price: {
                        currency: result[0].currency_id,
                        amount: result[0].price
                    },
                    picture: result[0].pictures[0].url,
                    condition: result[0].condition,
                    free_shipping: result[0].shipping.free_shipping,
                    sold_quantity: result[0].sold_quantity,
                    description: result[1].plain_text
                }
            };
            res.send(payload);
        } catch (e) {
            res.status(400).send(e)
        }
    } else {
        res.status(400).send("id param required")
    }
});


module.exports = router;