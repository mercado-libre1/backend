# ML Backend App

## Instalar dependencias

```
yarn // npm install
```

## Correr la app

```
yarn dev // npm run dev
```

## Descripción

La aplicación expone dos endpoints para el uso de la aplicacion de frontend

/api/items => devuelve una lista de items filtrada por el query param "q"

/api/items/:id => devuelve datos sobre un item con id == :id
