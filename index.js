const express = require('express');
const app = express();
const morgan = require('morgan');
const itemsController = require('./controllers/items');

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use('/api/items', itemsController);

app.listen(3001, () => {
    console.log("Meli Server started succesfully")
});